import sys
from constraint import Problem, AllDifferentConstraint

COORD_TYPE = tuple[int, int]
DOMAIN = list(range(1, 10))
COORDS = [(x, y) for x in range(9) for y in range(9)]


def load_grid(path: str) -> list[list[int | None]]:
    """ returns grid like [[None, None, 1, ...], [2, 4, None, ...], ...] """
    lines = open(path).read().splitlines()
    return [[None if char == '_' else int(char) for char in line] for line in lines]


def print_solution(solution: dict[COORD_TYPE, int]):
    for row, col in COORDS:
        print(solution[(row, col)], end='\n' if col == 8 else '')


def get_constraint_groups() -> list[list[COORD_TYPE]]:
    """ returns a list of coordinate-groups, in which each value must appear exactly once """
    rows = [[(x, y) for y in range(9)] for x in range(9)]
    cols = [[(y, x) for y in range(9)] for x in range(9)]
    blocks = [[(x+i, y+j) for i in range(3) for j in range(3)]
              for x in range(0, 9, 3)
              for y in range(0, 9, 3)]
    return [*rows, *cols, *blocks]


def main():
    rows = load_grid(sys.argv[1])
    prob = Problem()
    # define variables as 2D coordinate-tuples (0,0) to (8,8)
    for row, col in COORDS:
        allowed_vals = DOMAIN if rows[row][col] == None else [rows[row][col]]
        prob.addVariable((row, col), allowed_vals)
    # add constraints: in each constraint group, all variables have different values
    for constraint_group in get_constraint_groups():
        prob.addConstraint(AllDifferentConstraint(), constraint_group)
    if (solution := prob.getSolution()):
        print_solution(solution)
    else:
        print('puzzle has no solution')

if __name__ == "__main__":
    main()
