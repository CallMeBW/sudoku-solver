Sudoku Solver
=============

This Python program solves Sudoku puzzles using constraint programming.

Requirements
------------

This program requires Python 3.8 and the `constraint` module. You can install the `constraint` module using the following command:


`pip install python-constraint`

Usage
-----

To use the program, run the following command:


`python sudoku.py <path-to-puzzle-file>`

The `path-to-puzzle-file` argument should be the path to a file containing a Sudoku puzzle. The file should be formatted such that each row of the puzzle is on a separate line, with empty cells represented by underscores (`_`).

For example, a puzzle input may look like this:

```
__5__839_
_3_______
___7___8_
__45__6_2
61_______
2___49___
_____24_5
__9_8____
56_______
```

Output
------

If a solution exists, the program will print the solution to the puzzle to the console. If no solution exists, the program will print a message indicating that the puzzle has no solution.